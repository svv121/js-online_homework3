"use strict";
/*
1. Цикли використовуються щоб не дублювати код багаторазово, коли є необхідність повторювати дії, обчислення до тих пір, поки дотримується певна умова.
2. Цикл while буде виконуватися доти, доки вірно (істинно) вираз, переданий йому параметром.
Цикл for містить 3 частини, 1 - встановлює змінну перед початком циклу (наприклад, i = 0), 2 - визначає умову для виконання циклу, 3 - змінюєє значення змінної ( наприклад i += 1) щоразу, коли виконується блок коду в циклі.
3. Явне приведення (перетворенням) - це коли розробник здійснює намір сконвертувати значення одного типу до значення іншого типу, записуючи це відповідним чином в коді. Неявне перетворення типів - це автоматичний процес перетворення значень з одного типу до іншого.
*/
let m = "";
let n = "";
while (!Number.isInteger(m) || !Number.isInteger(n) || m < 0 || n <= m) {
    m = +prompt("Please enter first positive integer (m) \n (a positive number without a fractional component):", "");
    n = +prompt("Please enter second positive integer (n)  \n it should be more than first integer (m):", "");
    if (!Number.isInteger(m) || !Number.isInteger(n) || m < 0 || n <= m) {
        alert("Error! Please be more attentive this time while entering numbers.");
    }
}
console.log(`Integers multiple of 5 in the range from 0 to ${m}:`);
for (let i = 5; i <= m; i += 5) {
    console.log(i);
}
if (m < 5) {
    console.log("Sorry, no numbers");
}
console.log(`Prime numbers in the range from ${m} to ${n}:`);
const isPrime = (num) => {
    if (num < 2) {
        return false;
    }
    let divider = 2;
    while (divider <= num / 2) {
        if (num % divider === 0) {
            return false;
        }
        divider += 1;
    }
    return true;
}
for (let i = m; i <= n; i++) {
    if (isPrime(i)){
        console.log(i);
    }
}